package com.example.tp1_3_javareal;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import static android.widget.LinearLayout.VERTICAL;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LinearLayout layout = new LinearLayout(this);
        layout.setOrientation(VERTICAL);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
        LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.MATCH_PARENT);
        layout.setLayoutParams(layoutParams);

        EditText firstName = new EditText(this);
        firstName.setEms(10);
        firstName.setHint(getResources().getString(R.string.firstName));
        firstName.setRawInputType(InputType.TYPE_TEXT_VARIATION_PERSON_NAME);
        LinearLayout.LayoutParams layout_450 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT);
        firstName.setLayoutParams(layout_450);
        layout.addView(firstName);

        EditText lastName = new EditText(this);
        lastName.setEms(10);
        lastName.setHint(getResources().getString(R.string.lastName));
        lastName.setRawInputType(InputType.TYPE_TEXT_VARIATION_PERSON_NAME);
        LinearLayout.LayoutParams layout_504 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT);
        layout_504.width = LinearLayout.LayoutParams.MATCH_PARENT;
        layout_504.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        lastName.setLayoutParams(layout_504);
        layout.addView(lastName);

        EditText age = new EditText(this);
        age.setEms(10);
        age.setHint(getResources().getString(R.string.age));
        age.setRawInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_VARIATION_PASSWORD);
        LinearLayout.LayoutParams layout_256 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT);
        layout_256.width = LinearLayout.LayoutParams.MATCH_PARENT;
        layout_256.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        age.setLayoutParams(layout_256);
        layout.addView(age);

        EditText competence = new EditText(this);
        competence.setEms(10);
        competence.setHint(getResources().getString(R.string.competence));
        competence.setRawInputType(InputType.TYPE_CLASS_TEXT);
        LinearLayout.LayoutParams layout_544 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT);
        layout_544.width = LinearLayout.LayoutParams.MATCH_PARENT;
        layout_544.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        competence.setLayoutParams(layout_544);
        layout.addView(competence);

        EditText phone = new EditText(this);
        phone.setEms(10);
        phone.setHint(getResources().getString(R.string.phone));
        phone.setRawInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_VARIATION_PASSWORD);
        LinearLayout.LayoutParams layout_466 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT);
        phone.setLayoutParams(layout_466);
        layout.addView(phone);

        Button button = new Button(this);
        button.setText(getResources().getString(R.string.valider));
        LinearLayout.LayoutParams layout_653 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT);
        button.setLayoutParams(layout_653);
        layout.addView(button);

        String string = getString(R.string.toasted);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.this, string, Toast.LENGTH_LONG).show();
            }
        });
        setContentView(layout);
    }
}